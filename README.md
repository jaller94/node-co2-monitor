# node-co2-monitor

Read CO₂ concentration, temperature and humidity from TFA Dostmann AirCO2NTROL Mini & Coach.

## Contents

* [Supported Hardware](#supported-hardware)
* [Install](#install)
* [Getting started](#getting-started)
* [API](#api)
    * [Methods](#methods)
    * [Events](#events)
* [Credits](#credits)
* [License](#license)


## Supported Hardware

* [TFA Dostmann AirCO2NTROL Mini - Monitor CO2 31.5006](https://www.tfa-dostmann.de/produkt/co2-monitor-airco2ntrol-mini-31-5006/)
* [TFA Dostmann AirCO2NTROL Coach - Monitor CO2 31.5009](https://www.tfa-dostmann.de/en/produkt/co2-monitor-airco2ntrol-coach-31-5009/)


## Install

```bash
npm install @jaller94/node-co2-monitor
```

## Getting started

```javascript
'use strict';
const CO2Monitor = require('@jaller94/node-co2-monitor');

const monitor = new CO2Monitor();

// Connect device.
monitor.connect((err) => {
    if (err) {
        return console.error(err.stack);
    }
    console.log('Monitor connected.');

    // Read data from CO2 monitor.
    monitor.transfer();
});

// Get results.
monitor.on('temp', (temperature) => {
    console.log(`temp: ${ temperature }`);
});
monitor.on('co2', (co2) => {
    console.log(`co2: ${ co2 }`);
});
monitor.on('hum', (humidity) => {
    console.log(`hum: ${ humidity }`);
});

// Error handler
monitor.on('error', (err) => {
    console.error(err.stack);
    // Disconnect device
    monitor.disconnect(() => {
        console.log('Monitor disconnected.');
        process.exit(0);
    });
});
```


## API
### Methods
#### new CO2Monitor(options) -> Object
Create CO2Monitor instance.

#### monitor.connect(Function callback)
Setup usb connection to CO2 monitor.

#### monitor.disconnect(Function callback)
Close device connection.

#### monitor.transfer([Function callback])
Start data transfer from CO2 monitor.

#### monitor.temperature -> Number
Get latest Ambient Temperature (Tamb) in ℃.

#### monitor.co2 -> Number
Get latest Relative Concentration of CO2 (CntR) in ppm.

#### monitor.humidity -> Number
Get latest Relative Humidity of CO2 in relative humidity * 100. Only the Coach supports this value. The Mini will report 0.

### Events

#### temp -> Number
Triggered by temperature update with Ambient Temperature (Tamb) in ℃.

#### co2 -> Number
Triggered by co2 update with Relative Concentration of CO2 (CntR) in ppm.

#### hum -> Number
Triggered by hum update with Relative Humidity of CO2 in relative humidity * 100. Only the Coach supports this value. The Mini will report 0.

#### error -> Error
Triggered by error.


## Projects using node-co2-monitor

* [CO2 Monitor Exporter](https://github.com/huhamhire/co2-monitor-exporter) - Prometheus exporter for CO2 concentration and indoor temperature from TFA Dostmann AirCO2NTROL Mini.


## Credits

Inspired by Henryk Plötz:
[Reverse-Engineering a low-cost USB CO₂ monitor](https://hackaday.io/project/5301-reverse-engineering-a-low-cost-usb-co-monitor/log/17909-all-your-base-are-belong-to-us).


## License

MIT

## Troubleshooting

### LIBUSB_ERROR_ACCESS

If an error is thrown that contains `LIBUSB_ERROR_ACCESS` your current user isn't allowed to directly interact with the USB device.

The full error might look like this:

```
node-co2-monitor/node_modules/usb/usb.js:38
	this.__open()
	     ^

Error: LIBUSB_ERROR_ACCESS
    at Device.usb.Device.open (node-co2-monitor/node_modules/usb/usb.js:38:7)
    at CO2Monitor.connect (node-co2-monitor/co2_monitor.js:48:22)
    at Object.<anonymous> (node-co2-monitor/demo.js:7:9)
    at Module._compile (node:internal/modules/cjs/loader:1105:14)
    at Object.Module._extensions..js (node:internal/modules/cjs/loader:1159:10)
    at Module.load (node:internal/modules/cjs/loader:981:32)
    at Function.Module._load (node:internal/modules/cjs/loader:822:12)
    at Function.executeUserEntryPoint [as runMain] (node:internal/modules/run_main:77:12)
    at node:internal/main/run_main_module:17:47 {
  errno: -3
}
```

#### Just commands

```bash
echo 'SUBSYSTEM=="usb", ATTRS{idVendor}=="04d9", ATTRS{idProduct}=="a052", GROUP="plugdev", MODE="0666"' | sudo tee /etc/udev/rules.d/98-co2mon.rules
sudo groupadd plugdev
sudo usermod -a -G plugdev $(whoami)
sudo sudo udevadm control --reload-rules && sudo udevadm trigger
```

#### Explanation

Create a udev rule with the following content. This vendor and product id will work for both the Mini and Coach model. It will allow every user in the `plugdev` group to access the usb device.

```
SUBSYSTEM=="usb", ATTRS{idVendor}=="04d9", ATTRS{idProduct}=="a052", GROUP="plugdev", MODE="0666"
```

Ensure the group `plugdev` exists and that your current user is a member of that group.

Now run `sudo udevadm control --reload-rules && sudo udevadm trigger` and re-plug the USB cable of the CO₂ Monitor.
